﻿using SRMP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SRMPServer
{
    public class SRMPServer : MultiProtocolPacketServer
    {

        public GameState state;

        public SRMPServer(int port) : this(new TcpListener(IPAddress.Any,port),port)
        {
            Listener.Start();
        }

        public SRMPServer(TcpListener listener, int listenerport) : base(listener, new SRMPUdpServer(listenerport))
        {
            (UdpServer as SRMPUdpServer).server = this;
        }

        public override void OnClientPacketReceived(ConnectedClient client, object packet)
        {
            state?.OnTcpPacket(client, packet);
        }

        public override void OnClientConnect(ConnectedClient client)
        {
            state?.OnClientConnected(client.ClientID);
        }

        public override void OnClientDisconnect(ConnectedClient client)
        {
            base.OnClientDisconnect(client);
            state?.OnClientDisconnected(client.ClientID);
        }
    }

    public class SRMPUdpServer : UdpPacketServer
    {
        public SRMPServer server;
        public SRMPUdpServer(int listeningPort) : base(listeningPort)
        {
        }

        public override void OnPacketReceived(ConnectedClient client, object packet)
        {
            server?.state?.OnUdpPacket(client, packet);
        }
    }
}
