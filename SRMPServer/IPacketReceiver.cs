﻿using SRMP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRMPServer
{
    public interface IPacketReceiver
    {
        void OnTcpPacketReceived(PacketServer.ConnectedClient client, object packet);

        void OnUdpPacketReceived(UdpPacketServer.ConnectedClient client, object packet);
    }
}
