﻿using MonomiPark.SlimeRancher.Persist;
using SRMP.Shared;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMPServer
{
    public class ActorTracker : PacketReceiver
    {
        public Dictionary<long, TrackedActor> Actors = new Dictionary<long, TrackedActor>();
        public ActorTracker(GameState state) : base(state)
        {
        }

        public void ProcessActorDatas(IEnumerable<ActorDataV09> data)
        {
            foreach(var v in data)
            {
                var h = GetActor(v);
                if (h.ActorID >= State.nextActorId)
                {
                    State.nextActorId = h.ActorID + 1;
                }
            }
        }
        
        public void OnUdpPacketReceived(int id, object packet)
        {
            switch (packet)
            {
                case ActorSyncPacket sync:
                    TrySyncActor(sync, id);
                    break;
            }
        }

        public void OnTcpPacketReceived(int id, object packet)
        {
            switch (packet)
            {
                case ActorDestructionPacket destroy:
                    if (Actors.Remove(destroy.ActorID)) DestructionList.Add(destroy.ActorID);

                    break;
                case ActorSpawnPacket spawn:
                    
                    DestructionList.Remove(spawn.ActorID);
                    var data = new ActorDataV09();
                    using(var v = new MemoryStream(spawn.Data))
                    {
                        data.Load(v);
                    }
                    data.actorId = spawn.ActorID;
                    
                    UpdateActorSimulator(GetActor(data), id);
                    
                    break;
                case ActorSyncStateUpdatePacket sync:

                    var actor = GetActor(sync.ActorID);
                    if (sync.State == ActorSyncStateUpdatePacket.SyncState.SIMULATING) UpdateActorSimulator(actor, id);

                    break;
                case ClientStateUpdatePacket clientstate:
                    if (clientstate.State != ClientState.LOADED) break;
                    foreach (var v in Actors)
                    {
                        var dat = v.Value.GetActorData();
                        using (var stream = new MemoryStream())
                        {
                            dat.Write(stream);
                            State.Server.SendPacketToClient(id, new ActorSpawnPacket()
                            {
                                ActorID = v.Value.ActorID,
                                IdentifiableId = ((Identifiable.Id)dat.typeId).ToString(),
                                Data = stream.ToArray()
                            });
                        }
                    }

                    State.Server.SendPacketToClient(id, new ClientStateUpdatePacket()
                    {
                        State = ClientState.READY
                    });

                    break;
            }
        }

        public void UpdateActorSimulator(TrackedActor actor, int simulator)
        {
            actor.CurrentSimulator = simulator;
            //Console.WriteLine($"Actor {actor.ActorID} updated its simulator to {simulator}");
            foreach (var v in State.Server.ConnectedClients)
            {
                State.Server.SendPacketToClient(v.ClientID, new ActorSyncStateUpdatePacket() { ActorID = actor.ActorID, State = v.ClientID == simulator ? ActorSyncStateUpdatePacket.SyncState.SIMULATING : ActorSyncStateUpdatePacket.SyncState.TRACKING });
            }
            
        }
        
        public TrackedActor GetActor(ActorDataV09 data)
        {
            var a = TrackedActor.GetFromData(data);
            Actors[data.actorId] = a;
            return a;
        }

        public TrackedActor GetActor(long id)
        {
            if (!Actors.TryGetValue(id, out var actor))
            {
                actor = new TrackedActor();
                actor.ActorID = id;
                Console.WriteLine($"Got untracked actor {id}");
                Actors[id] = actor;
            }
            return actor;
        }

        public HashSet<long> DestructionList = new HashSet<long>();

        public void TrySyncActor(ActorSyncPacket packet, int sender)
        {

            if (DestructionList.Contains(packet.ActorId))
            {
                Console.WriteLine("Trying to sync a deleted actor " + packet.ActorId);
                State.Server.SendPacketToClient(sender, new ActorDestructionPacket() { ActorID = packet.ActorId });
                return;
            }
            if (!Actors.ContainsKey(packet.ActorId))
            {
                Console.WriteLine("Tried to sync a non existent actor " + packet.ActorId);
                return;
            }
            var actor = GetActor(packet.ActorId);
            if (actor.CurrentSimulator == 0) UpdateActorSimulator(actor, sender);
            if (actor.CurrentSimulator == sender)
            {
              
                actor.UpdateFromPacket(packet,sender);
                SendActorUpdate(actor);
            }
        }

        public void SendActorUpdate(long id) => SendActorUpdate(GetActor(id));

        public void SendActorUpdate(TrackedActor actor)
        {
            var packet = actor.ToPacket();
            foreach(var v in State.Server.UdpServer.ConnectedClients)
            {
                if (actor.CurrentSimulator == v.ClientID) continue;
                State.Server.UdpServer.SendPacket(v, packet);
            }
        }

        public override void OnTcpPacketReceived(PacketServer.ConnectedClient client, object packet)
        {
            OnTcpPacketReceived(client.ClientID, packet);
        }

        public override void OnUdpPacketReceived(UdpPacketServer.ConnectedClient client, object packet)
        {
            OnUdpPacketReceived(client.ClientID, packet);
        }

        public class TrackedActor
        {
            public long ActorID;
            public Vector3 Position;
            public Vector3 Velocity;
            public Quaternion Rotation;
            public Vector3 AngularVelocity;
            public bool IsKinematic;
            public bool UsesGravity;

            public ActorDataV09 RawActorData;

            public int CurrentSimulator;

            public ActorSyncPacket ToPacket()
            {
                return new ActorSyncPacket()
                {
                    ActorId = ActorID,
                    Position = Position,
                    Velocity = Velocity,
                    AngularVelocity = AngularVelocity,
                    isKinematic = IsKinematic,
                    usesGravity = UsesGravity,
                    Rotation = Rotation
                };
            }

            public static TrackedActor GetFromData(ActorDataV09 a)
            {
                var actor = new TrackedActor();
                actor.ActorID = a.actorId;
                actor.RawActorData = a;
                actor.Position = a.pos.value;
                actor.Rotation = EulerAngleToQuaternion(a.rot.value);
                return actor;
            }

            public ActorDataV09 GetActorData()
            {
                RawActorData.actorId = ActorID;
                RawActorData.pos = new Vector3V02() { value = Position };
                RawActorData.rot = new Vector3V02() { value = QuatToEulerSafe(Rotation) };
                return RawActorData;
            }

            public static Vector3 QuatToEulerSafe(Quaternion a)
            {
                return new Vector3((float)Math.Atan2(2*(a.w*a.x+a.y*a.z),1-2*(a.x*a.x+a.y*a.y)),(float)Math.Asin(2*(a.w*a.y-a.z*a.x)),(float)Math.Atan2(2*(a.w*a.z+a.x*a.y),1-2*(a.y*a.y+a.z*a.z)))*57.29578f;
            }

            public static Quaternion EulerAngleToQuaternion(Vector3 a)
            {
                a /= 57.29578f;
                return new Quaternion(0,0,(float)Math.Sin(a.z/2),(float)Math.Cos(a.z/2))*new Quaternion(0,(float)Math.Sin(a.y/2),0,(float)Math.Cos(a.y/2))*new Quaternion((float)Math.Sin(a.x/2),0,0,(float)Math.Cos(a.x/2));
            }

            public void UpdateFromPacket(ActorSyncPacket packet,int simulator)
            {
                ActorID = packet.ActorId;
                Position = packet.Position;
                Velocity = packet.Velocity;
                Rotation = packet.Rotation;
                AngularVelocity = packet.AngularVelocity;
                IsKinematic = packet.isKinematic;
                UsesGravity = packet.usesGravity;
                
            }
        }
    }
}
