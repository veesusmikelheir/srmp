﻿using MonomiPark.SlimeRancher.Persist;
using SRMP.Shared;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;

namespace SRMPServer
{
    public class GameState
    {
        public SRMPServer Server;
        public ActorTracker ActorTracker;
        public long nextActorId;

        public GameV11 SavedGame = new GameV11();

        public Dictionary<int, int> RealIdsToFakeIds = new Dictionary<int, int>();

        List<IPacketReceiver> PacketReceivers = new List<IPacketReceiver>();

        public GameState(SRMPServer server)
        {
            this.Server = server;
            Server.state = this;
            ActorTracker = new ActorTracker(this);
            Timer timer = new Timer(1000);
            timer.Elapsed += (a, b) =>
            {
                foreach(var v in Server.UdpServer.ConnectedClients)
                {
                    Server.UdpServer.SendPacket(v, new UdpKeepAlivePacket());
                }
            };
            timer.AutoReset = true;
            timer.Start();
        }

        public GameState(int port) : this(new SRMPServer(port))
        {

        }

        public void RegisterPacketReceiver(IPacketReceiver receiver)
        {
            PacketReceivers.Add(receiver);
        }

        

        public void OnClientConnected(int id)
        {
            SendGame(id);
            RealIdsToFakeIds[id] = Server.GetID();
        }

        public void OnClientDisconnected(int id)
        {
            TcpSendToAllExcept(new ClientDisconnectPacket() { Client = RealIdsToFakeIds[id] }, id);
            RealIdsToFakeIds.Remove(id);
        }

        public void SendGame(int id)
        {
            using(var stream = new MemoryStream())
            {
                var temp = SavedGame.actors;
                SavedGame.actors = new List<ActorDataV09>();
                SavedGame.Write(stream);
                SavedGame.actors = temp;
                Server.SendPacketToClient(id, new FileTransferPacket()
                {
                    Data = stream.ToArray()
                });
            }

            Server.SendPacketToClient(id, new ClientStateUpdatePacket()
            {
                State = ClientState.LOADING
            });
        }

        public void OnTcpPacket(PacketServer.ConnectedClient client, object packet)
        {
            if (packet is ActorSpawnPacket actorSpawn)
            {
                if (nextActorId > actorSpawn.ActorID)
                {
                    var originalActor = actorSpawn.ActorID;
                    actorSpawn.ActorID = nextActorId;

                    Server.SendPacketToClient(client, new ActorIdSwitchPacket() { OriginalActorId = originalActor, NewActorId = actorSpawn.ActorID });
                }
                if (actorSpawn.ActorID >= nextActorId)
                {
                    nextActorId = actorSpawn.ActorID + 1;
                }

            }
            foreach(var v in PacketReceivers)
            {
                v.OnTcpPacketReceived(client, packet);
            }
            if(PacketRegistry.ShouldRedirectToAllClients(packet)) TcpSendToAllExcept(packet,client);
        }

        public void OnUdpPacket(UdpPacketServer.ConnectedClient client, object packet)
        {
            foreach (var v in PacketReceivers)
            {
                v.OnUdpPacketReceived(client, packet);
            }
            switch (packet)
            {
                case PlayerPositionUpdatePacket player:
                    player.PlayerID = RealIdsToFakeIds[client.ClientID];
                    UdpSendToAllExcept(player, client.ClientID);
                    break;
            }
        }

        public void TcpSendToAllExcept(object packet, PacketServer.ConnectedClient client) => TcpSendToAllExcept(packet, client.ClientID);

        public void TcpSendToAllExcept(object packet, int outlier)
        {
            foreach(var v in Server.ConnectedClients)
            {
                if (outlier == v.ClientID) continue;
                Server.SendPacketToClient(v, packet);
            }
        }



        public void UdpSendToAllExcept(object packet, UdpPacketServer.ConnectedClient client) => UdpSendToAllExcept(packet, client.ClientID);

        public void UdpSendToAllExcept(object packet, int outlier)
        {
            foreach(var v in Server.UdpServer.ConnectedClients)
            {
                if (outlier == v.ClientID) continue;
                Server.UdpServer.SendPacket(v, packet);
            }
        }

        public const string SRMP_SAVE_NAME = "MULTIPLAYER_SRMP_SAVE";

        public void Initialize(string savefile)
        {
            Server.StartConnectingThread();
            Server.UdpServer.BeginListeningLoop();
            using (var v = new FileStream(savefile, FileMode.Open))
            {
                SavedGame.Load(v);
                SavedGame.displayName = SRMP_SAVE_NAME;
                SavedGame.gameName = SRMP_SAVE_NAME;
            }

            ActorTracker.ProcessActorDatas(SavedGame.actors);
        }
    }
}
