﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using SRMP.Shared;
using SRMP.Shared.Packets;
using System.IO;
using System.Threading;

namespace SRMPServer
{
    static class Program
    {
        static DateTime time;
        static void Main(string[] args)
        {
            SRMP.Shared.Initializer.Initialize();
            var gamestate = new GameState(25565);
            gamestate.Initialize("filetosend");
            return;
            var tcp = new TcpClient();
            tcp.Connect(new IPEndPoint(IPAddress.Loopback, 25565));
            var client = new TestClient(tcp.Client, new TestUdpClient(new IPEndPoint(IPAddress.Loopback, 25565), 0));
            client.StartListeningLoop();
            client.UdpClient.StartListeningLoop();
            client.SendUdpHandshake();

            var packet = new ActorSpawnPacket()
            {
                ActorID = 1,
                IdentifiableId = null,
                Data = new byte[10]
            };
            using (var v = new MemoryStream())
            {
                var writer = new BinaryWriter(v);
                PacketRegistry.TryWritePacket(writer, packet);


                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                var reader = new BinaryReader(v);

                PacketRegistry.TryReadPacket(reader, out var newPacket);
            }
        }

        public class TestUdpClient : UdpPacketClient
        {
            public TestUdpClient(IPEndPoint targetEndPoint, int listeningPort) : base(targetEndPoint, listeningPort)
            {
            }

            public override void OnPacket(object packet)
            {
                Console.WriteLine("Client got packet " + ID + " " + packet);
            }
        }

        public class TestUdpServer : UdpPacketServer
        {
            public TestUdpServer(int listeningPort) : base(listeningPort)
            {
            }
            int count = 0;
            public override void OnPacketReceived(ConnectedClient client, object packet)
            {
                if (client != ConnectedClients.First()) return;
                foreach (var v in ConnectedClients)
                {

                    if (v == client) continue;
                    SendPacket(v, packet);
                }
            }
        }

        public class TestServer : MultiProtocolPacketServer
        {


            public TestServer(TcpListener listener, UdpPacketServer server) : base(listener, server)
            {
            }

            public override void OnClientPacketReceived(ConnectedClient client, object packet)
            {
                switch (packet)
                {
                    case FileTransferPacket file:
                        FileTransferPacket.ProcessPacket(file, (x) =>
                        {
                            File.WriteAllBytes("receivedfile", x);
                            Console.WriteLine($"File took {(DateTime.Now - time).TotalSeconds} seconds");
                        });
                        break;
                   
                    default:
                        if (packet is ActorSyncPacket && ConnectedClients.First() != client) return;
                        foreach(var v in ConnectedClients)
                        {
                            if (v == client) continue;
                            SendPacketToClient(v, packet);
                        }
                        break;
                }


            }

            public override void RegisterClient(int id, Socket socket)
            {
                base.RegisterClient(id, socket);
                foreach (var v in FileTransferPacket.GetPackets(File.ReadAllBytes("filetosend")))
                {
                    SendPacketToClient(id, v);
                }
                Console.WriteLine("Sent file");
            }
        }

        public class TestClient : MultiProtocolPacketClient
        {

            public TestClient(Socket socket, UdpPacketClient client) : base(socket, client)
            {
            }

            protected override void OnPacketReceived(object packet)
            {
                Console.WriteLine(packet + " received by yay client " + Socket.LocalEndPoint);
            }
        }
    }
}
