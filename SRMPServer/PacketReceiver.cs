﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SRMP.Shared;

namespace SRMPServer
{
    public abstract class PacketReceiver : IPacketReceiver
    {
        public GameState State { get; protected set; }

        public PacketReceiver(GameState state)
        {
            this.State = state;
            state.RegisterPacketReceiver(this);
        }

        public abstract void OnTcpPacketReceived(PacketServer.ConnectedClient client, object packet);
        public abstract void OnUdpPacketReceived(UdpPacketServer.ConnectedClient client, object packet);
    }
}
