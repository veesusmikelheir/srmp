﻿using HarmonyLib;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Patches
{
    [HarmonyPatch(typeof(FileStorageProvider))]
    [HarmonyPatch("GetGameData")]
    internal static class FileStorageProviderGetGameDataPatch
    {
        internal static byte[] stream;
        public static bool Prefix(string name, MemoryStream dataStream)
        {
            if (name == SRMPClient.MULTIPLAYER_SAVE_NAME)
            {
                dataStream.Write(stream, 0, stream.Length);
                return false;
            }
            return true;
        }
    }
}
