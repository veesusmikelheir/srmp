﻿using SRML.SR.SaveSystem.Data.Ammo;
using SRMP.Shared.Packets.Ammo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRMP.Patches.AmmoPatches
{
    static class AmmoSelectedSlotPatches
    {
        public static void OnAmmoSlot(Ammo ammo)
        {
            if (!ClientManager.ReadyToSend) return;
            if(AmmoIdentifier.TryGetIdentifier(ammo,out var identifier))
            {
                if (identifier.AmmoType == AmmoType.PLAYER) return;
                ClientManager.Client.SendPacketToServer(new AmmoSelectedIdPacket()
                {
                    Identifier = identifier,
                    SelectedId = ammo.selectedAmmoIdx
                });
            }
        }
    }

    internal static class AmmoSetAmmoSlotPatch
    {
        public static void Postfix(bool __result, Ammo __instance, int idx)
        {
            if (__result) AmmoSelectedSlotPatches.OnAmmoSlot(__instance);
        }
    }
}
