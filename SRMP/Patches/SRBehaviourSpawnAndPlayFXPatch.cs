﻿using HarmonyLib;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using static SRMP.Shared.Utils.ObjectIdentifier;

namespace SRMP.Patches
{
    [HarmonyPatch(typeof(SRBehaviour))]
    [HarmonyPatch("SpawnAndPlayFX", new Type[] { typeof(GameObject), typeof(GameObject), typeof(Vector3), typeof(Quaternion)})]
    internal static class SRBehaviourSpawnAndPlayFXPatch
    {
        internal static List<string> queuedfx = new List<string>();
        public static void Postfix(GameObject prefab, GameObject parentObject, Vector3 position, Quaternion rotation)
        {
            if (!ClientManager.ReadyToSend) return;
            var fxId = prefab.name.Replace("(Instance)", "");
            if (queuedfx.Remove(fxId)) return;
            var identifiable = parentObject.GetComponent<Identifiable>();
            long id = identifiable ? identifiable.GetActorId() : 0;
            ClientManager.Client.SendPacketToServer(new FXSyncPacket()
            {
                FXName = fxId,
                ParentIdentifier = new Shared.Utils.ObjectIdentifier( SRBehaviour.dynamicParent == parentObject ? IdentifierType.OTHER : IdentifierType.ACTOR,id),
                Position = position,
                Rotation = rotation
            });
        }
    }
}
