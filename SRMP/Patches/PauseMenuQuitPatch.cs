﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRMP.Patches
{
    [HarmonyPatch(typeof(PauseMenu))]
    [HarmonyPatch("Quit")]
    internal static class PauseMenuQuitPatch
    {
        public static void Prefix()
        {
            ClientManager.Disconnect();
        }
    }
}
