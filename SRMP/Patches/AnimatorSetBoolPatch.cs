﻿using HarmonyLib;
using SRMP.Shared.Packets;
using SRMP.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP.Patches
{
    [HarmonyPatch(typeof(Animator))]
    [HarmonyPatch("SetBool",new Type[] { typeof(int),typeof(bool)})]
    internal static class AnimatorSetBoolPatch
    {
        public static List<Animator> animatorQueue = new List<Animator>();
        public static void Postfix(Animator __instance, int id, bool value)
        {
            if (animatorQueue.Remove(__instance)) return;
            if (ClientManager.ClientState != ClientState.READY) return;
            if (!ClientManager.Connected) return;
            var actor = __instance.GetComponentInParent<ActorSyncer>();


            ObjectIdentifier identifier = __instance.gameObject.GetIdentifier();

            if (actor)
            {

                if (actor.State != ActorSyncStateUpdatePacket.SyncState.SIMULATING) return;
            }
            else return;


            ClientManager.Client.SendPacketToServer(new AnimatorBoolSyncPacket()
            {
                AnimationID = id,
                State = value,
                Object = identifier
            });
        }
    }
}
