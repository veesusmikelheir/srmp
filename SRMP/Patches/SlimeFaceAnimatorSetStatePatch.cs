﻿using HarmonyLib;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP.Patches
{
    [HarmonyPatch(typeof(SlimeFaceAnimator))]
    [HarmonyPatch("SetState")]
    internal static class SlimeFaceAnimatorSetStatePatch
    {
        public static List<string> statequeue = new List<string>();
        public static void Prefix(SlimeFaceAnimator __instance, SlimeFaceAnimator.State state)
        {
            if (!ClientManager.ReadyToSend) return;
            string stateString = state.GetString();
            if (statequeue.Remove(stateString)) return;
            if (state == __instance.currState) return;
            if (stateString == null) Debug.Log(state + " is null!");


            if (!ActorSyncer.IsSimulating(__instance.gameObject)) return;

            var identifier = __instance.gameObject.GetIdentifier();

            ClientManager.Client.SendPacketToServer(new FaceStatePacket()
            {
                Object = identifier,
                State = stateString
            });
        }
    }
}
