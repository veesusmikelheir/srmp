﻿using MonomiPark.SlimeRancher.DataModel;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using static SRMP.Shared.Packets.ActorSyncStateUpdatePacket;

namespace SRMP
{
    public class ActorSyncer : MonoBehaviour, ActorModel.Participant
    {
        public SyncState State = SyncState.TRACKING;

        public ActorSyncPacket NextPacket;
        public ActorSyncPacket PreviousPacket;

        public float LastPacketReceiveTime;

        public float NextPacketSendTime;

        public bool QueuedForDestroy;

        public static float UpdateInterval = .2f;

        Rigidbody body;
        internal ActorModel model;
        Vacuumable vac;
        SlimeSubbehaviourPlexer plexer;

        Dictionary<RegisteredActorBehaviour, bool> enabledStates = new Dictionary<RegisteredActorBehaviour, bool>();

        public void Awake()
        {
            body = GetComponent<Rigidbody>();
            vac = GetComponent<Vacuumable>();
            plexer = GetComponent<SlimeSubbehaviourPlexer>();
        }

        public void Start()
        {

            //SwitchState(State);
            if (!ClientManager.Connected) return;
            ClientManager.Client.UdpClient.SendPacket(GetCurrentPacket());
        }

        public void SwitchState(SyncState newState)
        {
            if (!ClientManager.ReadyToSend) return;

            UpdateSyncState(newState);
            
            ClientManager.Client.SendPacketToServer(new ActorSyncStateUpdatePacket() { ActorID = model.actorId, State = State });

        }



        public void FixedUpdate()
        {
            if (ClientManager.ClientState != ClientState.READY) return;
            if (model == null||(vac&&vac.destroyOnVac))
            {
                // how did we even get to this point!?
                Destroy(gameObject);
                return;
            }

            if ((Time.time - LastPacketReceiveTime)>5 && !Simulating && !sentError)
            {
                Debug.LogError($"Actor {model.actorId} hasn't received a position update in {(Time.time - LastPacketReceiveTime)} seconds!");
                LastPacketReceiveTime = Time.time;
            }

            switch (State)
            {
                case SyncState.SIMULATING:
                    if (Time.time >= NextPacketSendTime)
                    {
                        NextPacketSendTime = Time.time + UpdateInterval;
                        if (!ClientManager.Connected) return;
                        ClientManager.Client.UdpClient.SendPacket(GetCurrentPacket());
                    }
                    break;
                case SyncState.TRACKING:
                    if (NextPacket.ActorId == 0 || PreviousPacket.ActorId == 0) return;
                    float t = (Time.time - LastPacketReceiveTime) / (UpdateInterval / 2);
                    if (vac && (vac.isTornadoed||vac.isHeld()||vac.isCaptive()))
                    {
                        SwitchState(SyncState.SIMULATING);
                    }
                    if (t > 1) return;
                    var expectedPacket = ActorSyncPacket.Lerp(PreviousPacket, NextPacket, Mathf.Clamp01(t));
                    //  var actualPacket = GetCurrentPacket();

                    ApplyPacket(expectedPacket);
                    break;
            }
        }

        public void OnCollisionEnter(Collision coll)
        {
            if (State == SyncState.SIMULATING) return;
            var otherSyncer = coll.gameObject.GetComponent<ActorSyncer>();
            var identifiable = coll.gameObject.GetComponent<Identifiable>();
            if ((identifiable && Identifiable.Id.PLAYER == identifiable.id)||(otherSyncer&&otherSyncer.State == SyncState.SIMULATING))
            {
                SwitchState(SyncState.SIMULATING);
            }
        }

        bool sentError;

        public void OnReceivePacket(ActorSyncPacket packet)
        {
            PreviousPacket = GetCurrentPacket();
            NextPacket = packet;
            LastPacketReceiveTime = Time.time;
            sentError = false;
        }

        

        public void UpdateSyncState(SyncState newState)
        {
            if(newState != State)
            {
                switch (newState)
                {
                    case SyncState.SIMULATING:
                        foreach (var v in GetComponentsInChildren<RegisteredActorBehaviour>(true))
                        {
                            v.enabled = true;
                        }
                        enabledStates.Clear();
                        if (plexer && plexer.behaviorBlockers >= 0) plexer.UnregisterBehaviorBlocker();
                        
                        break;
                    case SyncState.TRACKING:
                        enabledStates.Clear();
                        
                        foreach (var v in GetComponentsInChildren<RegisteredActorBehaviour>(true))
                        {
                            if (v == plexer||v == vac|| v is SlimeFaceAnimator) continue;
                            v.enabled = false;
                        }
                        if (plexer) plexer.RegisterBehaviorBlocker();
                        break;
                }
            }
            State = newState;

        }

        public bool Simulating => State == SyncState.SIMULATING;

        public static bool IsSimulating(GameObject obj)
        {
            var syncer = obj.GetComponent<ActorSyncer>();
            return syncer && syncer.Simulating;
        }

        public void InitModel(ActorModel model)
        {
        }

        public void SetModel(ActorModel model)
        {
            this.model = model;
        }

        public ActorSyncPacket GetCurrentPacket()
        {
            return new ActorSyncPacket()
            {
                ActorId = model.actorId,
                Velocity = body.velocity,
                AngularVelocity = body.angularVelocity,
                usesGravity = body.useGravity,
                isKinematic = body.isKinematic,
                Rotation = body.rotation,
                Position = body.position
            };
        }

        public void ApplyPacket(ActorSyncPacket packet)
        {
            body.velocity = packet.Velocity;
            body.angularVelocity = packet.AngularVelocity;
            body.isKinematic = packet.isKinematic;
            body.useGravity = packet.usesGravity;
            body.MovePosition(packet.Position);
            body.MoveRotation(packet.Rotation);
        }

        public void OnDestroy()
        {
            if (QueuedForDestroy||!ClientManager.Connected||ClientManager.ClientState!=ClientState.READY) return;
            QueuedForDestroy = true;
            ClientManager.Client.SendPacketToServer(new ActorDestructionPacket() { ActorID = model.actorId });
        }
    }
}
