﻿using HarmonyLib;
using MonomiPark.SlimeRancher;
using MonomiPark.SlimeRancher.DataModel;
using MonomiPark.SlimeRancher.Regions;
using SRML;
using SRML.SR;
using SRML.Utils;
using SRMP.Commands;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using UnityEngine;

namespace SRMP
{
    public class Main : ModEntryPoint
    {
        public static Thread mainThread;
        public override void Load()
        {
            GameContext.Instance.gameObject.AddComponent<MainThreadDispatcher>();
        }

        public override void PostLoad()
        {
            foreach(var v in GameContext.Instance.LookupDirector.GetAllPrefabs())
            {
                v.AddComponent<ActorSyncer>();
            }
        }

        internal static SavedGame game;

        public override void PreLoad()
        {
            game = new SavedGame(new MultiplayerPrefabInstantiator(),new SceneSavedGameInfoProvider(null, null));
            
            HarmonyInstance.PatchAll();
            Shared.Initializer.Initialize();
            SRML.Console.Console.RegisterCommand(new ConnectCommand());
            SRCallbacks.OnMainMenuLoaded += (x) =>
            {
                ClientManager.Disconnect();
            };
            SRCallbacks.OnActorSpawn += (x, y, z) =>
            {
                if (ClientManager.ClientState != ClientState.READY) return;
                try
                {
                    if (GameContext.Instance.AutoSaveDirector.IsLoadingGame()) return;
                    if (!ClientManager.Connected) return;
                    var data = game.BuildActorData(SceneContext.Instance.GameModel, (int)x, z.actorId, z, null);
                    if (data == null) return;
                    using (var stream = new MemoryStream())
                    {
                        data.Write(stream);
                        ClientManager.Client.SendPacketToServer(new ActorSpawnPacket() { Data = stream.ToArray(), IdentifiableId = x.ToString(), ActorID = z.actorId });
                    }
                }
                catch { }

            };

            SRCallbacks.OnSaveGameLoaded += (s) =>
            {
                s.Player.AddComponent<NetworkedPlayerManager>();

                if (!ClientManager.Connected) return;
                ClientManager.SetClientState(ClientState.LOADED);
                ClientManager.Client.SendPacketToServer(new ClientStateUpdatePacket()
                {
                    State = ClientState.LOADED
                });
            };
        }

        public class MultiplayerPrefabInstantiator : PrefabInstantiator
        {
            public GameObject InstantiateActor(long actorId, Identifiable.Id id, RegionRegistry.RegionSetId regionSetId, Vector3 pos, Vector3 rot, GameModel gameModel)
            {
                gameModel.nextActorId = Math.Max(gameModel.nextActorId, actorId + 1);
                return gameModel.InstantiateActor(actorId, GameContext.Instance.LookupDirector.GetPrefab(id), regionSetId, pos, Quaternion.Euler(rot), false, true);
            }

            public GameObject InstantiateGadget(Gadget.Id id, GadgetSiteModel site, GameModel gameModel)
            {
                throw new NotImplementedException();
            }

            public void InstantiatePlot(LandPlot.Id id, LandPlotModel plotModel, bool expectingPush)
            {
                throw new NotImplementedException();
            }
        }

    }

    
        
}
