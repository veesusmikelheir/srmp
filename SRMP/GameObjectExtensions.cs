﻿using SRMP;
using SRMP.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class GameObjectExtensions
{
    public static ObjectIdentifier GetIdentifier(this GameObject obj)
    {
        var actor = obj.GetComponentInParent<Identifiable>();
        if (actor) return new ObjectIdentifier(ObjectIdentifier.IdentifierType.ACTOR, Identifiable.GetActorId(actor.gameObject));
        var gadget = obj.GetComponentInParent<GadgetSite>();
        if (gadget) return new ObjectIdentifier(ObjectIdentifier.IdentifierType.GADGET, gadget.id);
        var landplot = obj.GetComponentInParent<LandPlotLocation>();
        if (landplot) return new ObjectIdentifier(ObjectIdentifier.IdentifierType.LANDPLOT, landplot.id);
        return default;
    }

    public static GameObject GetGameObject(this ObjectIdentifier identifier)
    {
        switch (identifier.Type)
        {
            case ObjectIdentifier.IdentifierType.ACTOR:
                return SceneContext.Instance.GameModel.GetModel(identifier.LongIdentifier.Value)?.transform.gameObject;
            case ObjectIdentifier.IdentifierType.GADGET:
                return SceneContext.Instance.GameModel.gadgetSites.TryGetValue(identifier.StringIdentifier, out var gadget) ? gadget.attached?.transform.gameObject ?? gadget.transform.gameObject : null;
            case ObjectIdentifier.IdentifierType.LANDPLOT:
                return SceneContext.Instance.GameModel.landPlots.TryGetValue(identifier.StringIdentifier, out var landplot) ? landplot.gameObj : null;
            default:
                return null;
        }
    }
}

