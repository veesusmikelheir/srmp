﻿using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP
{
    public class NetworkedPlayer : MonoBehaviour
    {
        float LastUpdateTime;

        PlayerPositionUpdatePacket NextPacket;
        PlayerPositionUpdatePacket PreviousPacket;


        public void FixedUpdate()
        {
            float t = Mathf.Clamp01((Time.time-LastUpdateTime)/NetworkedPlayerManager.UpdateInterval);
            var packet = PlayerPositionUpdatePacket.Lerp(PreviousPacket, NextPacket, t);
            ApplyPacket(packet);
        }

        public void OnPacketReceived(PlayerPositionUpdatePacket packet)
        {
            PreviousPacket = NextPacket;
            NextPacket = packet;
            LastUpdateTime = Time.time;
        }

        public void ApplyPacket(PlayerPositionUpdatePacket packet)
        {
            transform.position = packet.Position;
            transform.rotation = packet.Rotation;
        }
    }
}
