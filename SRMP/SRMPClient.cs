﻿using MonomiPark.SlimeRancher;
using MonomiPark.SlimeRancher.Persist;
using SRML.SR.Utils.BaseObjects;
using SRMP.Patches;
using SRMP.Shared;
using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using static SRMP.Shared.Utils.ObjectIdentifier;

namespace SRMP
{
    public class SRMPClient :MultiProtocolPacketClient
    {
        public const string MULTIPLAYER_SAVE_NAME = "MULTIPLAYER_SRMP_SAVE";

        public SRMPClient(Socket socket, UdpPacketClient client) : base(socket, client)
        {
        }

        public override bool SendPacketToServer(object packet)
        {
            return base.SendPacketToServer(packet);
        }

        protected override void OnDisconnect()
        {
            base.OnDisconnect();
            Application.Quit();
        }

        protected override void OnPacketReceived(object packet)
        {
            switch (packet)
            {
                case FileTransferPacket file:
                    FileTransferPacket.ProcessPacket(file, (x) =>
                    {
                        MainThreadDispatcher.ScheduleAction(() =>
                        {
                            FileStorageProviderGetGameDataPatch.stream = x;
                            GameContext.Instance.AutoSaveDirector.BeginLoad(MULTIPLAYER_SAVE_NAME, MULTIPLAYER_SAVE_NAME, () => Debug.Log("error"));
                        });
                    });
                    break;
                case ActorSpawnPacket actor:
                    var data = new ActorDataV09();
                    data.Load(new MemoryStream(actor.Data));
                    data.typeId = (int)Enum.Parse(typeof(Identifiable.Id), actor.IdentifiableId);
                    MainThreadDispatcher.ScheduleAction(() => {
                        Main.game.PushActorData(SceneContext.Instance.GameModel, data, null);
                    });
                    break;
                case ActorDestructionPacket destruction:
                    MainThreadDispatcher.ScheduleAction(() =>
                    {
                        var actorModel = SceneContext.Instance.GameModel.GetModel(destruction.ActorID);
                        
                        if (actorModel == null)
                        {
                            Debug.Log("Tried to deleted a null actor! " + destruction.ActorID);
                            return;
                        }
                        var syncer = actorModel.transform.GetComponent<ActorSyncer>();
                        if (syncer) syncer.QueuedForDestroy = true;
                        Destroyer.DestroyActor(actorModel.transform.gameObject,"ActorDestructionPacket",true);
                    });
                    break;
                case ActorSyncStateUpdatePacket sync:
                    MainThreadDispatcher.ScheduleAction(()=>
                        SceneContext.Instance.GameModel.GetModel(sync.ActorID)?.transform.GetComponent<ActorSyncer>()?.UpdateSyncState(sync.State)
                    );
                    break;
                case ActorIdSwitchPacket switchid:
                    var gamemodel = SceneContext.Instance.GameModel;
                    gamemodel.nextActorId = switchid.NewActorId + 1;
                    var model = gamemodel.GetModel(switchid.OriginalActorId);
                    if (model == null) return;
                    model.GetType().GetField("actorId").SetValue(model, switchid.NewActorId);
                    Debug.Log($"Old id: {switchid.OriginalActorId} {model.actorId} newid");
                    gamemodel.actors.Remove(switchid.OriginalActorId);
                    gamemodel.actors[switchid.NewActorId] = model;
                    break;
                case ClientDisconnectPacket disconnect:
                    NetworkedPlayerManager.Instance.OnDisconnect(disconnect.Client);
                    break;
                case FXSyncPacket fx:
                    MainThreadDispatcher.ScheduleAction(() =>
                    {
                        if(BaseObjects.originFXs.TryGetValue(fx.FXName, out var prefab))
                        {
                            SRBehaviourSpawnAndPlayFXPatch.queuedfx.Add(fx.FXName);
                            var actorModel = fx.ParentIdentifier.Type == Shared.Utils.ObjectIdentifier.IdentifierType.OTHER ? SRBehaviour.dynamicParent : (SceneContext.Instance.GameModel.GetModel(fx.ParentIdentifier.LongIdentifier.Value)?.transform.gameObject ?? SRBehaviour.dynamicParent);
                            if (fx.Position.HasValue) SRBehaviour.SpawnAndPlayFX(prefab, actorModel, fx.Position.Value, fx.Rotation.Value);
                            else SRBehaviour.SpawnAndPlayFX(prefab, actorModel);
                        }
                    });
                    break;
                case ClientStateUpdatePacket clientstate:
                    ClientManager.SetClientState(clientstate.State);
                    break;
                case AnimatorBoolSyncPacket anim:
                    if (ClientManager.ClientState != ClientState.READY) break;
                    MainThreadDispatcher.ScheduleAction(() =>
                    {
                        foreach (var animator in anim.Object.GetGameObject().GetComponentsInChildren<Animator>())
                        {
                            AnimatorSetBoolPatch.animatorQueue.Add(animator);
                            animator.SetBool(anim.AnimationID, anim.State);
                        }
                    });
                    break;
                case FaceStatePacket facestate:
                    {
                        MainThreadDispatcher.ScheduleAction(() =>
                        {
                            var gameobject = facestate.Object.GetGameObject();
                            var animator = gameobject.GetComponent<SlimeFaceAnimator>();
                            if (!animator) return;
                            var state = animator.GetState(facestate.State);
                            if (state == null) return;
                            SlimeFaceAnimatorSetStatePatch.statequeue.Add(facestate.State);
                            animator.SetState(state);
                        });
                    }
                    break;
            }
        }

       
    }

    public class SRMPUdpClient : UdpPacketClient
    {
        public SRMPUdpClient(IPEndPoint targetEndPoint, int listeningPort) : base(targetEndPoint, listeningPort)
        {
        }

        public override void OnPacket(object packet)
        {
            switch (packet)
            {
                case ActorSyncPacket sync:
                    MainThreadDispatcher.ScheduleAction(() =>
                    {
                        SceneContext.Instance.GameModel.GetModel(sync.ActorId)?.transform.GetComponent<ActorSyncer>()?.OnReceivePacket(sync);
                    });
                    break;
                case PlayerPositionUpdatePacket player:
                    NetworkedPlayerManager.Instance.OnPacketReceived(player);
                    break;
                case UdpKeepAlivePacket _:
                    if (ClientManager.Connected) ClientManager.Client.UdpClient.SendPacket(packet);
                    break;
            }
        }
    }
}
