﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using SRMP.Shared.Packets;
namespace SRMP
{
    public class NetworkedPlayerManager : SRSingleton<NetworkedPlayerManager>
    {
        float NextUpdateTime;

        public static float UpdateInterval = 0.05f;

        public Dictionary<int, NetworkedPlayer> Players = new Dictionary<int, NetworkedPlayer>();

        public override void Awake()
        {
            base.Awake();
        }

        public void FixedUpdate()
        {
            if (ClientManager.ClientState != ClientState.READY) return;
            if (!ClientManager.Connected) return;
            if (Time.time > NextUpdateTime)
            {
                NextUpdateTime = Time.time + UpdateInterval;
                ClientManager.Client.UdpClient.SendPacket(GetPacket());
            }
        }

        public void OnPacketReceived(PlayerPositionUpdatePacket packet)
        {
            if(!Players.TryGetValue(packet.PlayerID,out var p))
            {
                p = CreatePlayer();
                Players[packet.PlayerID] = p;
            }
            p.OnPacketReceived(packet);
        }

        public void OnDisconnect(int playerid)
        {
            if (Players.TryGetValue(playerid, out var p)) Destroy(p.gameObject);
            Players.Remove(playerid);
        }

        public PlayerPositionUpdatePacket GetPacket()
        {
            return new PlayerPositionUpdatePacket()
            {
                Position = Camera.main.transform.position,
                Rotation = Camera.main.transform.rotation
            };
        }

        public NetworkedPlayer CreatePlayer()
        {
            var gameobject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            gameobject.GetComponent<Collider>().enabled = false;
            return gameobject.AddComponent<NetworkedPlayer>();
        }
    }
}
