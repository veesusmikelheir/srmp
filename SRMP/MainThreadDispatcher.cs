﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP
{
    class MainThreadDispatcher : SRSingleton<MainThreadDispatcher>
    {
        private static object lockObject = new object();

        Queue<Action> scheduledActions = new Queue<Action>();

        public static void ScheduleAction(Action action)
        {
            lock (lockObject)
            {
                Instance.scheduledActions.Enqueue(action);
            }
        }

        public void Update()
        {
            lock (lockObject)
            {
                while (scheduledActions.Count > 0)
                {
                    try
                    {
                        scheduledActions.Dequeue()();
                    }
                    catch(Exception e)
                    {
                        Debug.LogError(e);
                    }
                }
            }
        }
    }
}
