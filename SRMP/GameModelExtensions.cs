﻿using MonomiPark.SlimeRancher.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRMP
{
    public static class GameModelExtensions
    {
        public static ActorModel GetModel(this GameModel model, long actorId)
        {
            if (!model.AllActors().ContainsKey(actorId)) return null;
            return model.GetActorModel(actorId);
        }
    }
}
