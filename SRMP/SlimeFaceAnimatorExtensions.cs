﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;


public static class SlimeFaceAnimatorExtensions
{

    static Dictionary<string, FieldInfo> StateGetters = new Dictionary<string, FieldInfo>();

    static SlimeFaceAnimatorExtensions()
    {
        foreach (var v in typeof(SlimeFaceAnimator).GetFields(BindingFlags.NonPublic|BindingFlags.Instance))
        {
            if (typeof(SlimeFaceAnimator.State).IsAssignableFrom(v.FieldType))
            {
                StateGetters[v.Name.ToLowerInvariant()] = v;
                
            }
        }
    }

    public static SlimeFaceAnimator.State GetState(this SlimeFaceAnimator animator, string face)
    {
        if (StateGetters.TryGetValue(face.ToLower(), out var field))
        {
            return field.GetValue(animator) as SlimeFaceAnimator.State;
        }
        return null;
    }

    public static string GetString(this SlimeFaceAnimator.State state)
    {
        foreach(var v in StateGetters)
        {
            if (v.Value.GetValue(state.anim) == state) return v.Key.ToUpperInvariant();
        }
        return null;
    }
}

