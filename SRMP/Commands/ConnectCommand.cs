﻿using SRML.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRMP.Commands
{
    public class ConnectCommand : ConsoleCommand
    {
        public override string ID => "connect";

        public override string Usage => "connect <ip>";

        public override string Description => "connects to a server";

        public override bool Execute(string[] args)
        {
            if (args == null) args = new string[] { "127.0.0.1" };
            ClientManager.Connect(args[0], 25565);
            return true;
        }
    }
}
