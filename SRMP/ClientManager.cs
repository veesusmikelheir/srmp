﻿using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

namespace SRMP
{
    public static class ClientManager
    {
        public static SRMPClient Client
        {
            get
            {
                return _client;
            }
        }
        static SRMPClient _client;

        public static ClientState ClientState { get; private set; }

        internal static void SetClientState(ClientState state)
        {
            ClientState = state;
        }

        public static bool Connected => Client?.Socket?.Connected ?? false;

        public static bool ReadyToSend => Connected && ClientState == ClientState.READY;

        public static void Connect(string ip, int port)
        {
            SetClientState(ClientState.NONE);
            _client = new SRMPClient(new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp), new SRMPUdpClient(new IPEndPoint(IPAddress.Parse(ip),port),port + UnityEngine.Random.Range(0,200)));
            Client.Socket.Connect(IPAddress.Parse(ip), port);
            
            Client.StartListeningLoop();
            Client.UdpClient.StartListeningLoop();
            Client.SendUdpHandshake();
        }

        public static void Disconnect()
        {
            SetClientState(ClientState.NONE);
            if (!ClientManager.Connected) return;
            Client?.Socket?.Disconnect(true);
        }


    }
}
