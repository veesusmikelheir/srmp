﻿using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SRMP.Shared
{
    public abstract class UdpPacketClient
    {
        public UdpClient Client;
        public int Port;
        public int ID = -1;
        public UdpPacketClient(IPEndPoint targetEndPoint, int listeningPort)
        {

            Client = new UdpClient();
            Client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            Client.Connect(targetEndPoint);
            Port = (Client.Client.LocalEndPoint as IPEndPoint).Port;
            Console.WriteLine(Port);
        }

        public abstract void OnPacket(object packet);

        public virtual UdpHandshakePacket GetHandshake()
        {
            var newShake = new UdpHandshakePacket();
            newShake.ID = ID;
            newShake.Address = IPAddress.Any;
            newShake.Port = Port;
            return newShake;
        }
        
        public virtual void ProcessHandshake(UdpHandshakePacket packet)
        {
            ID = packet.ID;
        }

        public void StartListeningLoop()
        {
            BeginListen();
        }

        public virtual bool SendPacket(object packet)
        {
            using(var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write(ID);
                if (PacketRegistry.TryWritePacket(writer,packet))
                {

                    Client.BeginSend(stream.GetBuffer(), (int)stream.Length, (x) =>
                    {
                        (x.AsyncState as UdpClient).EndSend(x);
                    },Client);
                    return true;
                }
                return false;
            }
        }

        protected virtual void BeginListen()
        {
            Client.BeginReceive(EndListen, new State(Client, new IPEndPoint(IPAddress.Any, 0)));
        }

        protected virtual void EndListen(IAsyncResult result)
        {
            var state = result.AsyncState as State;
            var client = state.Client;
            var endpoint = state.EndPoint;
            var bytes = client.EndReceive(result, ref endpoint);
            using (var stream = new MemoryStream(bytes))
            {
                var reader = new BinaryReader(stream);
                if(PacketRegistry.TryReadPacket(reader,out var packet))
                {
                    OnPacket(packet);
                }

            }
            BeginListen();
        }

        public class State
        {
            public UdpClient Client;
            public IPEndPoint EndPoint;
            public State(UdpClient client, IPEndPoint endpoint)
            {
                Client = client;
                EndPoint = endpoint;
            }
        }
    }
}
