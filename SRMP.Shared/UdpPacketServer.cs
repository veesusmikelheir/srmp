﻿using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SRMP.Shared
{
    public abstract class UdpPacketServer
    {


        public UdpClient Client;
        public List<ConnectedClient> ConnectedClients = new List<ConnectedClient>();



        public UdpPacketServer(int listeningPort)
        {
            Client = new UdpClient();
            Client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            Client.Client.Bind(new IPEndPoint(IPAddress.Any, listeningPort));
        }

        public virtual void BeginListeningLoop()
        {
            BeginListen();
        }

        public abstract void OnPacketReceived(ConnectedClient client, object packet);

        public virtual bool SendPacket(ConnectedClient client, object packet)
        {
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                if (PacketRegistry.TryWritePacket(writer, packet))
                {
                    Client.BeginSend(stream.GetBuffer(), (int)stream.Length, client.ReceivingEndPoint, (x) =>
                    {
                        (x.AsyncState as UdpClient).EndSend(x);
                    }, Client);
                    return true;
                }
                return false;
            }
        }

        public virtual bool SendPacket(int id, object packet)
        {
            return SendPacket(ConnectedClients.First(x => x.ClientID == id), packet);
        }

        public virtual ConnectedClient OnClientHandshake(UdpHandshakePacket packet,int id)
        {
            var newClient = new ConnectedClient();
            newClient.ClientID = id;
            newClient.ReceivingEndPoint = new IPEndPoint(packet.Address, packet.Port);
            ConnectedClients.Add(newClient);
            return newClient;
        }

        protected virtual void BeginListen()
        {
            Client.BeginReceive(EndListen, new State(Client, new IPEndPoint(IPAddress.Any, 0)));
        }


        protected virtual void EndListen(IAsyncResult result)
        {
            var state = result.AsyncState as State;
            var client = state.Client;
            var endpoint = state.EndPoint;
            var bytes = client.EndReceive(result, ref endpoint);
            //ReceivingClient.Send(new byte[1], 1,endpoint);
            using(var stream = new MemoryStream(bytes))
            {
                var reader = new BinaryReader(stream);
                var id = reader.ReadInt32();
                var existingClient = ConnectedClients.FirstOrDefault(x => x.ClientID == id);
                if(existingClient!=null) existingClient.ReceivingEndPoint = endpoint;
                if (PacketRegistry.TryReadPacket(reader,out object packet))
                {
                    
                    if (existingClient != null)
                    {

                        OnPacketReceived(existingClient, packet);
                    }
                    else
                    {
                        Console.WriteLine("Got unknown udp client id " + id);
                    }
                }
            }

            BeginListen();
        }



        public class ConnectedClient
        {
            public int ClientID;
            public IPEndPoint ReceivingEndPoint;


        }

        public class State
        {
            public UdpClient Client;
            public IPEndPoint EndPoint;
            public State(UdpClient client, IPEndPoint endpoint)
            {
                Client = client;
                EndPoint = endpoint;
            }
        }
    }
}
