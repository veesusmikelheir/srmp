﻿using SRMP.Shared.Packets.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP.Shared
{
    public static class PacketRegistry
    {
        internal static readonly HashSet<Type> TypesToRedirectStraightToOtherClients = new HashSet<Type>();

        internal static List<IPacketProcessor> packetProcessors = new List<IPacketProcessor>();
        public static void RegisterPacketProcessor(IPacketProcessor processor)
        {
            packetProcessors.Add(processor);
            if (processor.PacketType.GetCustomAttributes(true).Any(x => typeof(RedirectToAllClientsAttribute).IsAssignableFrom(x.GetType()))) TypesToRedirectStraightToOtherClients.Add(processor.PacketType);
        }

        public static bool ShouldRedirectToAllClients(object packet)
        {
            return TypesToRedirectStraightToOtherClients.Contains(packet.GetType());
        }

        public static bool TryWritePacket(BinaryWriter writer, object packet)
        {
            var e = packetProcessors.FirstOrDefault(x => x.IsMyPacket(packet));
            if (e == null) return false;
            writer.Write(e.ID);
            e.Write(writer, packet);
            return true;
        }

        public static bool TryReadPacket(BinaryReader reader, out object packet)
        {
            packet = null;
            long position = reader.BaseStream.Position;
            var id = reader.ReadString();
            var e = packetProcessors.FirstOrDefault(x => x.ID == id);
            if (e == null)
            {
                reader.BaseStream.Position = position;
                packet = id;
                return false;
            }
            packet = e.Read(reader);
            return true;
        }
    }
}
