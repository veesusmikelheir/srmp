﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

namespace SRMP.Shared
{
    public abstract class PacketClient : PacketSocket
    {

        public PacketClient(Socket socket) : base(socket)
        {
        }

        public virtual bool SendPacketToServer(object packet)
        {
            return SendPacket(packet);
        }

        protected override void OnDisconnect()
        {
            Console.WriteLine("Disconnected!");
        }
    }
}
