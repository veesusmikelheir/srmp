﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared
{
    public abstract class PacketProcessor<T> : IPacketProcessor
    {
        public abstract string ID { get; }
        public bool IsMyPacket(object packet)
        {
            return packet is T;
        }

        public virtual Type PacketType => typeof(T);

        public abstract T ReadPacket(BinaryReader reader);

        public object Read(BinaryReader reader)
        {
            return ReadPacket(reader);
        }

        public abstract void WritePacket(BinaryWriter writer, T packet);

        public void Write(BinaryWriter writer, object packet)
        {
            WritePacket(writer, (T)packet);
        }
    }

    public interface IPacketProcessor
    {
        string ID { get; }
        Type PacketType { get; }
        bool IsMyPacket(object packet);
        object Read(BinaryReader reader);
        void Write(BinaryWriter writer, object packet);
    }
}
