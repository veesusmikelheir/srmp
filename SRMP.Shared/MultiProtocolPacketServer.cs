﻿using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SRMP.Shared
{
    public abstract class MultiProtocolPacketServer : PacketServer
    {
        public virtual UdpPacketServer UdpServer { get; protected set; }

        protected Dictionary<ConnectedClient, int> TcpToUdp = new Dictionary<ConnectedClient, int>();

        public MultiProtocolPacketServer(TcpListener listener, UdpPacketServer server) : base(listener)
        {
            UdpServer = server;
        }



        protected override void PacketPreprocess(ConnectedClient client, object packet)
        {
            switch (packet)
            {
                case UdpHandshakePacket udphand:
                    udphand.Address = (client.Socket.RemoteEndPoint as IPEndPoint).Address;
                    var newClient = UdpServer?.OnClientHandshake(udphand,client.ClientID);
                    if (newClient == null) return;
                    udphand.ID = newClient.ClientID;
                    Console.WriteLine($"Client {newClient.ClientID} has connected at {udphand.Address}:{udphand.Port}");
                    SendPacketToClient(client, udphand);
                    TcpToUdp[client] = newClient.ClientID;
                    client.ClientID = newClient.ClientID;
                    break;
                default:
                    base.PacketPreprocess(client, packet);
                    break;
            }

        }

        protected override void DisconnectPreprocess(ConnectedClient client)
        {
            if (TcpToUdp.ContainsKey(client))
            {
                var otherClient = UdpServer.ConnectedClients.FirstOrDefault(x => x.ClientID == TcpToUdp[client]);
                UdpServer.ConnectedClients.Remove(otherClient);
                Console.WriteLine("Disconnecting " + TcpToUdp[client]);
            }
            base.DisconnectPreprocess(client);
        }

    }
}
