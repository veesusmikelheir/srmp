﻿using SRMP.Shared.Packets.Attributes;
using SRMP.Shared.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    [RedirectToAllClients]
    public class FaceMiscUpdatePacket
    {
        public ObjectIdentifier Object;
        public bool blush;
        public bool seekingFood;

        static FaceMiscUpdatePacket()
        {
            PacketRegistry.RegisterPacketProcessor(new FaceMiscUpdatePacketProcessor());
        }
    }

    public class FaceMiscUpdatePacketProcessor : PacketProcessor<FaceMiscUpdatePacket>
    {
        public override string ID => "facemisc";

        public override FaceMiscUpdatePacket ReadPacket(BinaryReader reader)
        {
            var packet = new FaceMiscUpdatePacket();
            packet.Object = ObjectIdentifier.Read(reader);
            packet.blush = reader.ReadBoolean();
            packet.seekingFood = reader.ReadBoolean();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, FaceMiscUpdatePacket packet)
        {
            ObjectIdentifier.Write(writer, packet.Object);
            writer.Write(packet.blush);
            writer.Write(packet.seekingFood);
        }
    }
}
