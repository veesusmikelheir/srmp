﻿using SRML.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP.Shared.Packets
{
    public struct ActorSyncPacket
    {
        public long ActorId;
        public Vector3 Position;
        public Vector3 Velocity;
        public Quaternion Rotation;
        public Vector3 AngularVelocity;
        public bool usesGravity;
        public bool isKinematic;
        
        static ActorSyncPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new ActorSyncPacketProcessor());
        }

        public static ActorSyncPacket Lerp(ActorSyncPacket a, ActorSyncPacket b, float t)
        {
            return new ActorSyncPacket()
            {
                ActorId = a.ActorId,
                Position = Vector3.Lerp(a.Position, b.Position, t),
                Velocity = Vector3.Lerp(a.Velocity, b.Velocity, t),
                AngularVelocity = Vector3.Lerp(a.AngularVelocity, b.AngularVelocity, t),
                Rotation = Quaternion.Slerp(a.Rotation, b.Rotation,t),
                isKinematic = t > .5 ? b.isKinematic : a.isKinematic,
                usesGravity = t > .5 ? b.usesGravity : a.usesGravity
            };
        }
    }

    public class ActorSyncPacketProcessor : PacketProcessor<ActorSyncPacket>
    {
        public override string ID => "as";

        public override ActorSyncPacket ReadPacket(BinaryReader reader)
        {
            var packet = new ActorSyncPacket();
            packet.ActorId = reader.ReadInt64();
            packet.Position = BinaryUtils.ReadVector3(reader);
            packet.Velocity = BinaryUtils.ReadVector3(reader);
            packet.Rotation = BinaryUtils.ReadQuaternion(reader);
            packet.AngularVelocity = BinaryUtils.ReadVector3(reader);
            packet.usesGravity = reader.ReadBoolean();
            packet.isKinematic = reader.ReadBoolean();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, ActorSyncPacket packet)
        {
            writer.Write(packet.ActorId);
            BinaryUtils.WriteVector3(writer, packet.Position);
            BinaryUtils.WriteVector3(writer, packet.Velocity);
            BinaryUtils.WriteQuaternion(writer,packet.Rotation);
            BinaryUtils.WriteVector3(writer, packet.AngularVelocity);
            writer.Write(packet.usesGravity);
            writer.Write(packet.isKinematic);
        }
    }
}
