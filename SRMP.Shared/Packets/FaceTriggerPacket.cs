﻿using SRMP.Shared.Packets.Attributes;
using SRMP.Shared.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    [RedirectToAllClients]
    public class FaceTriggerPacket
    {
        public ObjectIdentifier Object;
        public string Trigger;

        static FaceTriggerPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new FaceTriggerPacketProcessor());
        }
    }

    public class FaceTriggerPacketProcessor : PacketProcessor<FaceTriggerPacket>
    {
        public override string ID => "facetrig";

        public override FaceTriggerPacket ReadPacket(BinaryReader reader)
        {
            var packet = new FaceTriggerPacket();
            packet.Object = ObjectIdentifier.Read(reader);
            packet.Trigger = reader.ReadString();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, FaceTriggerPacket packet)
        {
            ObjectIdentifier.Write(writer, packet.Object);
            writer.Write(packet.Trigger);
        }
    }
}
