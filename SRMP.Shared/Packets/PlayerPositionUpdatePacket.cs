﻿using SRML.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP.Shared.Packets
{
    public struct PlayerPositionUpdatePacket
    {
        public int PlayerID;
        public Vector3 Position;
        public Quaternion Rotation;

        static PlayerPositionUpdatePacket()
        {
            PacketRegistry.RegisterPacketProcessor(new PlayerPositionUpdatePacketProcessor());
        }

        public static PlayerPositionUpdatePacket Lerp(PlayerPositionUpdatePacket a, PlayerPositionUpdatePacket b, float t)
        {
            return new PlayerPositionUpdatePacket()
            {
                PlayerID = a.PlayerID,
                Position = Vector3.Lerp(a.Position, b.Position, t),
                Rotation = Quaternion.Slerp(a.Rotation, b.Rotation, t)
            };
        }
    }

    public class PlayerPositionUpdatePacketProcessor : PacketProcessor<PlayerPositionUpdatePacket>
    {
        public override string ID => "pu";

        public override PlayerPositionUpdatePacket ReadPacket(BinaryReader reader)
        {
            var packet = new PlayerPositionUpdatePacket();
            packet.PlayerID = reader.ReadInt32();
            packet.Position = BinaryUtils.ReadVector3(reader);
            packet.Rotation = BinaryUtils.ReadQuaternion(reader);
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, PlayerPositionUpdatePacket packet)
        {
            writer.Write(packet.PlayerID);
            BinaryUtils.WriteVector3(writer, packet.Position);
            BinaryUtils.WriteQuaternion(writer, packet.Rotation);
        }
    }
}
