﻿using SRML.Utils;
using SRMP.Shared.Packets.Attributes;
using SRMP.Shared.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP.Shared.Packets
{
    [RedirectToAllClients]
    public class FXSyncPacket
    {
        public string FXName;
        public Vector3? Position;
        public Quaternion? Rotation;
        public ObjectIdentifier ParentIdentifier;

        static FXSyncPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new FXSyncPacketProcessor());
        }
    }

    public class FXSyncPacketProcessor : PacketProcessor<FXSyncPacket>
    {
        public override string ID => "fxsync";

        public override FXSyncPacket ReadPacket(BinaryReader reader)
        {
            var packet = new FXSyncPacket();
            packet.FXName = reader.ReadString();
            if (reader.ReadBoolean()) packet.Position = BinaryUtils.ReadVector3(reader);
            if (reader.ReadBoolean()) packet.Rotation = BinaryUtils.ReadQuaternion(reader);
            packet.ParentIdentifier = ObjectIdentifier.Read(reader);
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, FXSyncPacket packet)
        {
            writer.Write(packet.FXName);
            writer.Write(packet.Position.HasValue);
            if (packet.Position.HasValue) BinaryUtils.WriteVector3(writer,packet.Position.Value);
            writer.Write(packet.Rotation.HasValue);
            if (packet.Rotation.HasValue) BinaryUtils.WriteQuaternion(writer, packet.Rotation.Value);
            ObjectIdentifier.Write(writer, packet.ParentIdentifier);
        }
    }
}
