﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace SRMP.Shared.Packets
{
    public class UdpHandshakePacket
    {
        public int ID;
        public IPAddress Address;
        public int Port;

        static UdpHandshakePacket()
        {
            PacketRegistry.RegisterPacketProcessor(new UdpHandshakePacketProcessor());
        }
    }

    public class UdpHandshakePacketProcessor : PacketProcessor<UdpHandshakePacket>
    {
        public override string ID => "udp";

        public override UdpHandshakePacket ReadPacket(BinaryReader reader)
        {
            var packet = new UdpHandshakePacket();
            packet.Address = new IPAddress(reader.ReadBytes(reader.ReadInt32()));
            packet.Port = reader.ReadInt32();
            packet.ID = reader.ReadInt32();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, UdpHandshakePacket packet)
        {
            var bytes = packet.Address.GetAddressBytes();
            writer.Write(bytes.Length);
            writer.Write(bytes);
            writer.Write(packet.Port);
            writer.Write(packet.ID);
        }
    }
}
