﻿using SRML.Utils;
using SRMP.Shared.Packets.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SRMP.Shared.Packets
{
    [RedirectToAllClients]
    public class ActorSpawnPacket
    {
        public byte[] Data = new byte[0];
        public string IdentifiableId;
        public long ActorID;
        
        static ActorSpawnPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new ActorSpawnPacketProcessor());
        }
    }

    public class ActorSpawnPacketProcessor : PacketProcessor<ActorSpawnPacket>
    {
        public override string ID => "spawn";

        public override ActorSpawnPacket ReadPacket(BinaryReader reader)
        {
            var packet = new ActorSpawnPacket();
            packet.IdentifiableId = reader.ReadString();
            packet.ActorID = reader.ReadInt64();
            int counter = reader.ReadInt32();
            packet.Data = reader.ReadBytes(counter);

            return packet;
        }

        public override void WritePacket(BinaryWriter writer, ActorSpawnPacket packet)
        {
            writer.Write(packet.IdentifiableId);
            writer.Write(packet.ActorID);
            writer.Write(packet.Data.Length);
            writer.Write(packet.Data);
        }
    }
}
