﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static SRMP.Shared.Packets.ClientStateUpdatePacket;

namespace SRMP.Shared.Packets
{
    public class ClientStateUpdatePacket
    {
        public ClientState State;
        static ClientStateUpdatePacket()
        {
            PacketRegistry.RegisterPacketProcessor(new ClientStateUpdatePacketProcessor());
        }
    }
    public enum ClientState
    {
        NONE,
        LOADING,
        LOADED,
        READY
    }
    public class ClientStateUpdatePacketProcessor : PacketProcessor<ClientStateUpdatePacket>
    {
        public override string ID => "cstate";

        public override ClientStateUpdatePacket ReadPacket(BinaryReader reader)
        {
            return new ClientStateUpdatePacket()
            {
                State = (ClientState)reader.ReadInt32()
            };
        }

        public override void WritePacket(BinaryWriter writer, ClientStateUpdatePacket packet)
        {
            writer.Write((int)packet.State);
        }
    }
}
