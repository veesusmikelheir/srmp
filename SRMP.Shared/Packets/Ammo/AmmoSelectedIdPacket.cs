﻿using SRML.SR.SaveSystem.Data.Ammo;
using SRMP.Shared.Packets.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets.Ammo
{
    [RedirectToAllClients]
    public class AmmoSelectedIdPacket
    {
        public AmmoIdentifier Identifier;
        public int SelectedId;

        static AmmoSelectedIdPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new AmmoSelectedIdPacketProcessor());
        }
    }

    public class AmmoSelectedIdPacketProcessor : PacketProcessor<AmmoSelectedIdPacket>
    {
        public override string ID => "ammosel";

        public override AmmoSelectedIdPacket ReadPacket(BinaryReader reader)
        {
            var packet = new AmmoSelectedIdPacket();
            packet.Identifier = AmmoIdentifier.Read(reader);
            packet.SelectedId = reader.ReadInt32();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, AmmoSelectedIdPacket packet)
        {
            AmmoIdentifier.Write(packet.Identifier, writer);
            writer.Write(packet.SelectedId);
        }
    }
}
