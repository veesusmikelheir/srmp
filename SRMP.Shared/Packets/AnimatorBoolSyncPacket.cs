﻿using SRMP.Shared.Packets.Attributes;
using SRMP.Shared.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    [RedirectToAllClients]
    public class AnimatorBoolSyncPacket
    {
        public ObjectIdentifier Object;
        public int AnimationID;
        public bool State;

        static AnimatorBoolSyncPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new AnimatorBoolSyncPacketProcessor());
        }
    }

    public class AnimatorBoolSyncPacketProcessor : PacketProcessor<AnimatorBoolSyncPacket>
    {
        public override string ID => "anim";

        public override AnimatorBoolSyncPacket ReadPacket(BinaryReader reader)
        {
            var packet = new AnimatorBoolSyncPacket();
            packet.Object = ObjectIdentifier.Read(reader);
            packet.AnimationID = reader.ReadInt32();
            packet.State = reader.ReadBoolean();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, AnimatorBoolSyncPacket packet)
        {
            ObjectIdentifier.Write(writer, packet.Object);
            writer.Write(packet.AnimationID);
            writer.Write(packet.State);
        }
    }
}
