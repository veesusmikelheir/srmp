﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    public class ActorIdSwitchPacket
    {
        public long OriginalActorId;
        public long NewActorId;

        static ActorIdSwitchPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new ActorIdSwitchPacketProcessor());
        }
    }

    public class ActorIdSwitchPacketProcessor : PacketProcessor<ActorIdSwitchPacket>
    {
        public override string ID => "actorid";

        public override ActorIdSwitchPacket ReadPacket(BinaryReader reader)
        {
            var packet = new ActorIdSwitchPacket();
            packet.OriginalActorId = reader.ReadInt64();
            packet.NewActorId = reader.ReadInt64();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, ActorIdSwitchPacket packet)
        {
            writer.Write(packet.OriginalActorId);
            writer.Write(packet.NewActorId);
        }
    }
}
