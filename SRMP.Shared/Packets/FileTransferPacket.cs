﻿using SRML.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static SRMP.Shared.PacketClient;
using static SRMP.Shared.Packets.FileTransferPacket;

namespace SRMP.Shared.Packets
{
    public class FileTransferPacket
    {
        public byte[] Data = new byte[0];

        static FileTransferPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new FileTransferPacketProcessor());
        }

        public static IEnumerable<FileTransferPacket> GetPackets(byte[] bytes)
        {
            yield return new FileTransferPacket() { Data = bytes };
            /*
            yield return new FileTransferPacket() { Type = PacketType.START };
            int bytesLeft = bytes.Length;
            int segmentLength = 256;
            while (bytesLeft > segmentLength)
            {

                var newPacket = new FileTransferPacket();
                newPacket.Type = PacketType.DATA;
                newPacket.Data = new byte[segmentLength];
                Array.Copy(bytes, bytes.Length - bytesLeft, newPacket.Data, 0,segmentLength);
                yield return newPacket;
                bytesLeft -= segmentLength;
            }
            if (bytesLeft > 0)
            {

                var newPacket = new FileTransferPacket();
                newPacket.Type = PacketType.DATA;
                newPacket.Data = new byte[bytesLeft];
                Array.Copy(bytes, bytes.Length - bytesLeft, newPacket.Data, 0, bytesLeft);
                yield return newPacket;
            }
            yield return new FileTransferPacket() { Type = PacketType.END };
            */
        }

        static MemoryStream pendingStream;

        public static void ProcessPacket(FileTransferPacket packet, Action<byte[]> onFinished)
        {
            onFinished(packet.Data);
        }
    }

    public class FileTransferPacketProcessor : PacketProcessor<FileTransferPacket>
    {
        public override string ID => "file";

        public override FileTransferPacket ReadPacket(BinaryReader reader)
        {
            var packet = new FileTransferPacket();
            int count = reader.ReadInt32();
            packet.Data = reader.ReadBytes(count);
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, FileTransferPacket packet)
        {
            writer.Write(packet.Data.Length);
            writer.Write(packet.Data);
        }
    }
}
