﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    public class UdpKeepAlivePacket
    {
        static UdpKeepAlivePacket()
        {
            PacketRegistry.RegisterPacketProcessor(new UdpKeepAlivePacketProcessor());
        }
    }

    public class UdpKeepAlivePacketProcessor : PacketProcessor<UdpKeepAlivePacket>
    {
        public override string ID => "kpa";

        public override UdpKeepAlivePacket ReadPacket(BinaryReader reader)
        {
            return new UdpKeepAlivePacket();
        }

        public override void WritePacket(BinaryWriter writer, UdpKeepAlivePacket packet)
        {
            
        }
    }
}
