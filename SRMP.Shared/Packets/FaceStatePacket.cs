﻿using SRMP.Shared.Packets.Attributes;
using SRMP.Shared.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    [RedirectToAllClients]
    public class FaceStatePacket
    {
        public ObjectIdentifier Object;
        public string State;

        static FaceStatePacket()
        {
            PacketRegistry.RegisterPacketProcessor(new FaceStatePacketProcessor());
        }
    }

    public class FaceStatePacketProcessor : PacketProcessor<FaceStatePacket>
    {
        public override string ID => "facestate";

        public override FaceStatePacket ReadPacket(BinaryReader reader)
        {
            var packet = new FaceStatePacket();
            packet.Object = ObjectIdentifier.Read(reader);
            packet.State = reader.ReadString();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, FaceStatePacket packet)
        {
            ObjectIdentifier.Write(writer,packet.Object);
            writer.Write(packet.State);
        }
    }
}
