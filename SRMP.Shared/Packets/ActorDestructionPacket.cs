﻿using SRMP.Shared.Packets.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    [RedirectToAllClients]
    public class ActorDestructionPacket
    {
        public long ActorID;
        static ActorDestructionPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new ActorDestructionPacketProcessor());
        }
    }

    public class ActorDestructionPacketProcessor : PacketProcessor<ActorDestructionPacket>
    {
        public override string ID => "delactor";

        public override ActorDestructionPacket ReadPacket(BinaryReader reader)
        {
            return new ActorDestructionPacket() { ActorID = reader.ReadInt64() };
        }

        public override void WritePacket(BinaryWriter writer, ActorDestructionPacket packet)
        {
            writer.Write(packet.ActorID);
        }
    }
}
