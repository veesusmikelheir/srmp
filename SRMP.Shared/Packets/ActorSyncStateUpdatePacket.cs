﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static SRMP.Shared.Packets.ActorSyncStateUpdatePacket;

namespace SRMP.Shared.Packets
{
    public class ActorSyncStateUpdatePacket
    {
        public long ActorID;
        public SyncState State;

        public enum SyncState
        {
            TRACKING,
            SIMULATING
        }

        static ActorSyncStateUpdatePacket()
        {
            PacketRegistry.RegisterPacketProcessor(new ActorSyncStateUpdatePacketProcessor());
        }
    }

    public class ActorSyncStateUpdatePacketProcessor : PacketProcessor<ActorSyncStateUpdatePacket>
    {
        public override string ID => "syncstate";

        public override ActorSyncStateUpdatePacket ReadPacket(BinaryReader reader)
        {
            var packet = new ActorSyncStateUpdatePacket();
            packet.ActorID = reader.ReadInt64();
            packet.State = (SyncState)reader.ReadInt32();
            return packet;
        }

        public override void WritePacket(BinaryWriter writer, ActorSyncStateUpdatePacket packet)
        {
            writer.Write(packet.ActorID);
            writer.Write((int)packet.State);
        }
    }
}
