﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Packets
{
    public class ClientDisconnectPacket
    {
        public int Client;

        static ClientDisconnectPacket()
        {
            PacketRegistry.RegisterPacketProcessor(new ClientDisconnectPacketProcessor());
        }
    }

    public class ClientDisconnectPacketProcessor : PacketProcessor<ClientDisconnectPacket>
    {
        public override string ID => "disconnect";

        public override ClientDisconnectPacket ReadPacket(BinaryReader reader)
        {
            return new ClientDisconnectPacket()
            {
                Client = reader.ReadInt32()
            };
        }

        public override void WritePacket(BinaryWriter writer, ClientDisconnectPacket packet)
        {
            writer.Write(packet.Client);
        }
    }
}
