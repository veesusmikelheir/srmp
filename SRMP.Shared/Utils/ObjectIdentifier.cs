﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SRMP.Shared.Utils
{
    public struct ObjectIdentifier
    {
        public IdentifierType Type;
        public long? LongIdentifier;
        public string StringIdentifier;
        public enum IdentifierType
        {
            ACTOR,
            GADGET,
            LANDPLOT,
            OTHER
        }

        public ObjectIdentifier(IdentifierType type)
        {
            LongIdentifier = null;
            StringIdentifier = null;
            Type = type;
        }

        public ObjectIdentifier(IdentifierType type,long identifier) : this(type)
        {
            LongIdentifier = identifier;
        }

        public ObjectIdentifier(IdentifierType type, string identifier) : this(type)
        {
            StringIdentifier = identifier;
        }

        public static void Write(BinaryWriter writer, ObjectIdentifier identifier)
        {
            writer.Write((int)identifier.Type);
            writer.Write(identifier.LongIdentifier.HasValue);
            if (identifier.LongIdentifier.HasValue) writer.Write(identifier.LongIdentifier.Value);
            writer.Write(identifier.StringIdentifier!=null);
            if (identifier.StringIdentifier != null) writer.Write(identifier.StringIdentifier);
        }

        public static ObjectIdentifier Read(BinaryReader reader)
        {
            var identifier = (IdentifierType)reader.ReadInt32();
            long? longidentifier = null;
            string stringidentifier = null;
            if (reader.ReadBoolean()) longidentifier = reader.ReadInt64();
            if (reader.ReadBoolean()) stringidentifier = reader.ReadString();
            return new ObjectIdentifier()
            {
                LongIdentifier = longidentifier,
                StringIdentifier = stringidentifier,
                Type = identifier
            };
        }
    }
}
