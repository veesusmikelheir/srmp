﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SRMP.Shared
{
    public abstract class PacketServer
    {

        public readonly List<ConnectedClient> ConnectedClients = new List<ConnectedClient>();

        public TcpListener Listener { get; }

        protected ManualResetEvent resetEvent = new ManualResetEvent(false);
        
        public PacketServer(TcpListener listener)
        {
            Listener = listener;
        }

        public Thread StartConnectingThread()
        {
            var thread = new Thread(BeginListen);
            thread.Start();
            return thread;
        }

        
 
        protected virtual void BeginListen()
        {
            try
            {
                while (true)
                {
                    resetEvent.Reset();
                    Listener.BeginAcceptSocket(EndListen, Listener);
                    resetEvent.WaitOne();
                }
            }
            finally
            {
                Listener.Stop();
            }
        }
        protected void EndListen(IAsyncResult result)
        {
            var listener = result.AsyncState as TcpListener;
            var client = listener.EndAcceptSocket(result);
            Console.WriteLine($"Client connected! {client.RemoteEndPoint}");
            RegisterClient(GetID(), client);
            resetEvent.Set();
        }

        static Random rand = new Random();

        public int GetID()
        {
            int id = 0;
            do
            {
                id = rand.Next();
            }
            while (ConnectedClients.Any(x => x.ClientID == id));
            return id;
        }

        public abstract void OnClientPacketReceived(ConnectedClient client, object packet);
        public virtual void OnClientConnect(ConnectedClient client) { }


        public virtual void RegisterClient(int id, Socket socket)
        {
            if (!socket.Connected) return;
            var newClient = new ConnectedClient(id, this, socket);
            newClient.StartListeningLoop();
            ConnectedClients.Add(newClient);
            OnClientConnect(newClient);
        }

        public virtual void OnClientDisconnect(ConnectedClient client)
        {
            ConnectedClients.Remove(client);
            try
            {
                Console.WriteLine($"Client disconnected! {client.Socket.RemoteEndPoint}");
            }
            catch { }
            client.Dispose();
        }

        protected virtual void DisconnectPreprocess(ConnectedClient client)
        {
            OnClientDisconnect(client);
        }

        public virtual bool SendPacketToClient(int clientId,object packet)
        {
            var v = ConnectedClients.FirstOrDefault(x => x.ClientID == clientId);
            if (v == null) return false;
            return SendPacketToClient(v, packet);
        }

        protected virtual void PacketPreprocess(ConnectedClient client, object packet)
        {
            OnClientPacketReceived(client, packet);
        }

        public virtual bool SendPacketToClient(ConnectedClient client,object packet)
        {
            return client.SendPacket(packet);
        }

        public class ConnectedClient : PacketSocket, IDisposable
        {
            public int ClientID;
            public PacketServer BelongingServer;
           
            public ConnectedClient(int id, PacketServer server, Socket socket) : base(socket)
            {
                ClientID = id;

                BelongingServer = server;
            }

            public void Dispose()
            {
                Socket.Close();
            }

            protected override void OnDisconnect()
            {
                BelongingServer.DisconnectPreprocess(this);
            }

            protected override void OnPacketReceived(object packet)
            {
                BelongingServer.PacketPreprocess(this,packet);
            }
        }
    }
}
