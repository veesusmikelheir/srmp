﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SRMP.Shared
{
    public abstract class PacketSocket
    {
        public virtual Socket Socket { get; protected set; }

        protected abstract void OnPacketReceived(object packet);
        protected abstract void OnDisconnect();

        protected virtual void PreDisconnect()
        {
            if (Socket.Connected) Socket.Close();
            OnDisconnect();
        }

        protected virtual void PrePacketReceived(object packet)
        {
            OnPacketReceived(packet);
        }

        public PacketSocket(Socket socket)
        {
            Socket = socket;
        }

        public virtual bool SendPacket(object packet)
        {
            using (var stream = new MemoryStream())
            {
                if (PacketRegistry.TryWritePacket(new BinaryWriter(stream), packet))
                {
                    byte[] realbuf;
                    long realLength;

                    using (var newStream = new MemoryStream())
                    {
                        var writer = new BinaryWriter(newStream);
                        writer.Write(stream.Length);
                        newStream.Write(stream.ToArray(), 0, (int)stream.Length);
                        realbuf = newStream.ToArray();
                        realLength = newStream.Length;
                    }

                    if(Socket.Connected)
                    Socket.BeginSend(realbuf, 0, (int)realLength, SocketFlags.None, (x) => {
                        try
                        {
                            (x.AsyncState as Socket).EndSend(x);
                        }
                        catch { }
                    }, Socket);
                    return true;
                }
                else
                {
                    Console.WriteLine("writing failure while sending!");
                }
                return false;
            }
        }

        protected virtual void OnRawDataReceived(byte[] buffer)
        {
            using (var v = new BinaryReader(new MemoryStream(buffer)))
            {
                try
                {
                    if (PacketRegistry.TryReadPacket(v, out var packet))
                    {
                        PrePacketReceived(packet);
                    }
                    else
                    {
                        Console.WriteLine("Reading failure on receiving! " + packet + " " + buffer.Length);
                    }
                }
                catch
                {
                    throw new Exception("READING PACKET FAILURE " + buffer.Length);
                }
            }
        }

        public virtual void StartListeningLoop()
        {
            BeginListen();
        }

        protected virtual void BeginListen()
        {
            var bytes = new byte[sizeof(long)];
            try
            {
                Socket.BeginReceive(bytes, 0, sizeof(long), SocketFlags.None, EndListen, bytes);
            }
            catch
            {
                PreDisconnect();
            }
        }

        protected virtual void EndListen(IAsyncResult result)
        {
            try
            {
                var state = result.AsyncState as byte[];
                int bytesRead;
                try
                {
                    bytesRead = Socket.EndReceive(result);
                }
                catch
                {
                    PreDisconnect();
                    return;
                }
                if (bytesRead > 0)
                {
                    // Chance we might have only received small part of the data, keep receiving
                    while (bytesRead < sizeof(long))
                    {
                        var bytesToAdd =  Socket.Receive(state, bytesRead, sizeof(long) - bytesRead, SocketFlags.None);
                        if(bytesToAdd == 0)
                        {
                            bytesRead = 0;
                            break;
                        }
                        bytesRead += bytesToAdd;
                    }
                }

                if (bytesRead != 0)
                {
                    long bytesToRead;
                    using (var tempReader = new BinaryReader(new MemoryStream(state)))
                    {
                        bytesToRead = tempReader.ReadInt64();
                    }
                    var buffer = new byte[bytesToRead];

                    if (bytesToRead == 0)
                    {
                        Console.WriteLine("Empty message received!");
                        BeginListen();
                        return;
                    }

                    do
                    {
                        var toRemove = Socket.Receive(buffer, (int)(buffer.Length - bytesToRead), (int)bytesToRead, SocketFlags.None);
                        if(toRemove == 0)
                        {
                            PreDisconnect();
                            return;
                        }
                        bytesToRead -= toRemove;
                    } while (bytesToRead > 0);
                    OnRawDataReceived(buffer);
                    BeginListen();
                }
                else PreDisconnect();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Socket.Close();
                PreDisconnect();
            }
        }
    }
}
