﻿using SRMP.Shared.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace SRMP.Shared
{
    public abstract class MultiProtocolPacketClient : PacketClient
    {
        public UdpPacketClient UdpClient { get; protected set; }
        public MultiProtocolPacketClient(Socket socket,UdpPacketClient client) : base(socket)
        {
            this.UdpClient = client;
        }

        public virtual void SendUdpHandshake()
        {
            SendPacketToServer(UdpClient.GetHandshake());
        }

        protected override void PrePacketReceived(object packet)
        {
            switch (packet)
            {
                case UdpHandshakePacket udp:
                    UdpClient?.ProcessHandshake(udp);
                    break;
                default:
                    base.PrePacketReceived(packet);
                    break;
            }

        }
    }
}
